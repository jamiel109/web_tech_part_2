/*
 *
 * Module: <name>
 * < short description here e.g. "This module implements main entry point...">
 *
 * Student Name: Jamie Lorenz
 * Student Number: 46039198
 *
 */
import{userSelect} from '/js/views.js';

function redrawposts(targetid, uimage) { 

  /*   let content = "<h2>API Test</h2><ul>";
    content += "<li><a href='/#'>Three Posts</a></li>";
    content += "<li><a href='/#'>Recent Posts</a></li>";
    content += "<li><a href='/#'>Popular Posts</a></li>";
    content += "<li><a href='/posts/1'>A Single Post</a></li>"; 
    content += "</ul>";

    // update the page
    document.getElementById("target").innerHTML = content;
}
 */
let target = document.getElementById(targetid);

    let template = Handlebars.compile(
                                document.getElementById("user_image_table").textContent
                                );

    let list = template({'uimage': uimage});
    console.log(list);

    target.innerHTML = list;
}

function load_images() {

    fetch('/js/sample.json')
    .then(
        function(response) {
            return response.json();
        }
    )
    .then(
        function(data){
            console.log(data.slice(0, 3));

            redrawposts("user_images", data.slice(0, 3));
            redrawposts("recent_popular", data.slice(0, 10))
            
        }
    )

}

window.onload = function() {
 load_images();   
 userSelect("about");
};


